import React from 'react';
import { Root, Routes } from 'react-static'
export { 
    Link, 
    Router
} from '@reach/router'
import './app.css';
import Home from './Home';
import SignIn from './SignIn';
import SignUp from './SignUp';

function App() {
    return (
      <Root>
        <div className="content">
            <React.Suspense fallback={<em>Loading...</em>}>
                <Routes/>
            </React.Suspense>
        </div>
      </Root>
    )
  }
  
  export default App